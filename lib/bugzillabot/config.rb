# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

require 'yaml'

module Bugzillabot
  # Configuration wrapper
  class Config
    class << self
      # Order matters for these.
      DEFAULT_LOAD_PATHS = [
        File.absolute_path(File.join(__dir__, '../../.config.yaml')),
        File.join(Dir.home, '.config/bugzillabot.yaml')
      ].freeze

      def load_paths
        @load_paths ||= DEFAULT_LOAD_PATHS
      end

      attr_writer :load_paths
    end

    attr_reader :path
    attr_reader :url
    attr_reader :api_key
    attr_reader :log_directory

    def initialize
      path = self.class.load_paths.find { |x| File.exist?(x) }
      unless path
        raise "Failed to find a config file in #{local_path} nor #{user_path}"
      end
      load(path)
    end

    def load(path)
      warn "Loading #{path}"
      @path = path
      type = ENV['PRODUCTION'] ? 'production' : 'testing'
      @data = YAML.load_file(path).fetch(type)
      @url = @data.fetch('url')
      @api_key = @data.fetch('api_key')
      @log_directory = @data.fetch('log_directory', nil)
    end

    def log?
      @log_directory
    end

    def bugzilla_url
      url.gsub('/rest', '')
    end
  end
end
