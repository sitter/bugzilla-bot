# frozen_string_literal: true

# SPDX-License-Identifier: LGPL-2.1-only OR LGPL-3.0-only OR LicenseRef-KDE-Accepted-LGPL
# SPDX-FileCopyrightText: 2018 Harald Sitter <sitter@kde.org>

require 'delegate'
require 'faraday'
require 'json'
require 'logger'
require 'uri'

module Bugzillabot
  # Connection adaptor.
  # This class wraps HTTP interactions for our purposes and adds general purpose
  # automation on top of the raw HTTP actions.
  class Connection < DelegateClass(Faraday::Connection)
    def initialize(url: Bugzillabot.config.url,
                   api_key: Bugzillabot.config.api_key)
      # params: { 'Bugzilla_api_key' => api_key }
      # headers: { 'X-BUGZILLA-API-KEY' => api_key }
      # FIXME: should query the version and use the headers on 6.0 and the
      #   params on 5.0
      @connection = Faraday.new(url: url,
                                params: { 'Bugzilla_api_key' => api_key },
                                headers: default_headers) do |c|
        @connection = c # so we can call members in here
        c.request(:url_encoded)
        if ENV['DEBUG']
          enable_logging_to_stdout
        elsif Bugzillabot.config.log?
          enable_logging_to_directory(Bugzillabot.config.log_directory)
        end
        c.adapter Faraday.default_adapter # make requests with Net::HTTP
      end

      super(@connection)
    end

    private

    def default_headers
      { 'Accept' => 'application/json',
        'Content-Type' => 'application/json' }
    end

    def enable_logging_to_stdout
      enable_logging(Logger.new(STDOUT))
    end

    def enable_logging_to_directory(directory)
      enable_logging(Logger.new("#{directory}/http.log", 10, 512 * 1024))
    end

    def enable_logging(logger)
      @connection.response(:logger, logger,
                           headers: false, bodies: true) do |log|
        log.filter(/(Bugzilla_api_key=)(\w+)/, '\1[REMOVED]')
        log.filter(/(X-BUGZILLA-API-KEY:) "(\w+)"/, '\1[REMOVED]')
      end
    end
  end
end
